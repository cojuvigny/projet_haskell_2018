{-# LANGUAGE OverloadedStrings #-}
-- |
-- Module      : Main
-- Copyright   : (c) Corentin Juvigny 2018
-- License     : BSD-style
--
-- Maintainer  : corentin.juvigny@ensiie.fr
-- Stability   : unsure
-- Portability : ~portable
--
module Main where

import           Control.Monad
import qualified Data.Map.Strict    as Map
import           System.Environment
import           System.Exit
import           System.IO
import           TravellingSalesman

main = do
    args <- getArgs
    let argc = length args
    when (argc /= 2) $ do
        hPutStrLn stderr $ hlred "Error : "
                           ++ "invalid number of args : 2 expected, "
                           ++ show argc
                           ++ " given"
        exitFailure
    (initflag, incorpflag, optiflag) <- readFormatFile (args!!0)
    (cities, grph) <- readCityFile (args!!1)
    seed <- randomSeedDraw (length cities)
    when (null cities) $ do
        hPutStrLn stderr $ hlred "Error : "
                           ++ "invalid number of cities, at least one is required"
        exitFailure
    if grphIsNull grph
       then printTour
            . optimizeTour optiflag
            . getTour
            $ finishTour' incorpflag
                          seed
                          (newWorkingTour initflag cities :: WorkingTour Tour City)
       else printTour
            . optimizeTour optiflag
            . getTour
            $ finishTour' incorpflag
                          seed
                          (newWorkingTour' initflag grph cities :: WorkingTourGraph TourGraph City)
