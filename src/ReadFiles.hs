{-# LANGUAGE ScopedTypeVariables #-}
-- |
-- Module      : ReadFiles
-- Copyright   : (c) Corentin Juvigny 2018
-- License     : BSD-style
--
-- Maintainer  : corentin.juvigny@ensiie.fr
-- Stability   : unsure
-- Portability : ~portable
--
--
-- Usefull functions used for reading city files and set up files.
--

module ReadFiles ( readCityFile
                 , readFormatFile
                 , hlred
                 , hlgreen
                 ) where

import           City
import           Control.Exception
import           Control.Monad
import           Data.ByteString       ( ByteString )
import qualified Data.ByteString.Char8 as BC
import           Data.List
import qualified Data.Map.Strict       as Map
import           Data.Maybe
import qualified Data.Set              as Set
import           Data.Tuple.Extra
import           System.Exit
import           System.IO
import           Text.Read
import           Tour

-- | Colour the text in red.
hlred :: String -> String
hlred s = "\x1B[1m\x1B[31m" ++ s ++ "\x1B[0m"

-- | Colour the text in green.
hlgreen :: String -> String
hlgreen s = "\x1B[1m\x1B[32m" ++ s ++ "\x1B[0m"

-- | Create a list of cites out of the Handle file and a Graph of bytestrings.
--   If the graph can't be created, it returns an empty graph
readingCityFile :: IsCity c => Handle -> IO ([c],Graph ByteString)
readingCityFile h = do
    n <- read <$> hGetLine h
    cities <- replicateM n $ do
        line <- BC.words <$> BC.hGetLine h
        when (length line /= 3) (error "readingCityFile : Unsupported file format")
        let rd = read . BC.unpack
        return $ newCity (line!!0) (rd (line!!1)) (rd (line!!2))
    graph <- catch (readGraphFile h n) (\(e :: SomeException) -> return $ Graph Map.empty)
    return (cities,graph)

-- | Create a Graph representing all the roads between cities.
readGraphFile :: Handle -> Int -> IO (Graph ByteString)
readGraphFile h n = do
    grph <- fmap Map.fromList $ replicateM n $ do
               line <- BC.words <$> BC.hGetLine h
               let start = head line
               let last  = Set.fromList $ drop 2 line
               return (start,last)
    return (Graph grph)

-- | Read the file and extract a list of cities from it.
--   Exit the program if the format type is unsupported or if an error occurs.
readCityFile :: IsCity c => String -> IO ([c],Graph ByteString)
readCityFile name =
    catches (withFile name ReadMode readingCityFile)
            [ Handler (\e -> do let err = show (e :: IOException)
                                hPutStrLn stderr ((hlred "Error :") ++ " Couldn't open " ++ name ++ ": " ++ err)
                                exitFailure
                      )
            , Handler (\e -> do let err = show (e :: ErrorCall)
                                hPutStrLn stderr ((hlred "Error :") ++ " Couldn't read " ++ name ++ ": " ++ err)
                                exitFailure
                      )
            ]

-- | Read the file and extract a list of properties, one per line in the file.
--   Exit the program if an error occurs.
readFormatFile :: String -> IO (InitFlag,IncorporationFlag,OptimisationFlag)
readFormatFile name =
    catch (readsFormat <$> BC.lines <$> BC.readFile name)
          (\(e :: SomeException) ->
                hPutStrLn stderr ((hlred "Error :") ++ " Couldn't open " ++ name ++ ": " ++ show e)
                >> exitFailure)

-- | Return a tuple containing formats.
--   Throw an error if format can't be read.
readsFormat :: [ByteString] -> (InitFlag,IncorporationFlag,OptimisationFlag)
readsFormat t
    | length t == 3 = if test then err
                              else (fromJust fs,fromJust sn,fromJust th)
    | otherwise     = err
    where fs   = readMaybe . BC.unpack $ t!!0 :: Maybe InitFlag
          sn   = readMaybe . BC.unpack $ t!!1 :: Maybe IncorporationFlag
          th   = readMaybe . BC.unpack $ t!!2 :: Maybe OptimisationFlag
          test = fs == Nothing || sn == Nothing || th == Nothing
          err  = error $ (hlred "Error : ") ++ "format file invalid"
