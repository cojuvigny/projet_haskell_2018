{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE OverloadedStrings      #-}

-- |
-- Module      : City
-- Copyright   : (c) Corentin Juvigny 2018
-- License     : BSD-style
--
-- Maintainer  : corentin.juvigny@ensiie.fr
-- Stability   : unsure
-- Portability : ~portable
--
--
-- Structure representing a city.
-- A city is composed of a name, a longitude and a latitude.
--

module City where

import Data.ByteString.Char8 ( ByteString )

--Interface

type Point = (Double,Double)

class (Eq c,Ord c) => IsCity c where
    -- | Return the longitudinal coordonate of the city c
    longitude   :: c -> Double
    -- | Return the latitudinal coordonate of the city c
    latitude    :: c -> Double
    -- | Return the city name
    name        :: c -> ByteString
    -- | Create a new City from a name, a longitude and a latitude
    newCity     :: ByteString -> Double -> Double -> c
    -- | Return the euclidian distance between the two cities
    distance    :: c -> c -> Double
    distance va vb = sqrt ((longitude vb - longitude va) ^ 2 + (latitude vb - latitude va) ^ 2)
    {-# INLINE distance #-}
    -- | Return the euclidian distance between a city and a point
    distance'   :: c -> Point -> Double
    distance' v (x,y) = sqrt ((longitude v - x) ^ 2 + (latitude v - y) ^ 2)
    {-# INLINE distance' #-}
    -- | Return the cross product of 3 vectors of cities
    --   \[
    --      ccw (a,b,c) = (xb - xa) * (yc - ya) - (xc - xa) * (yb - ya)
    --   \]
    ccw         :: c -> c -> c -> Double
    ccw va vb vc = (longitude vb - longitude va) * (latitude vc - latitude va) - (longitude vc - longitude va) * (latitude vb - latitude va)
    {-# INLINE ccw #-}
    -- | Return the difference between the distance between c1 and c2
    --   and the sum of distance between c1 and c3 and between c2 and c3
    --   \[
    --      ccd(v1,v2,v3) = (distance(v1,v3) + distance(v2,v3)) - distance(v1,v2)
    --   \]
    ccd         :: c -> c -> c -> Double
    ccd v1 v2 v3 = (distance v1 v3 + distance v2 v3) - distance v1 v2
    {-# INLINE ccd #-}

data City = City { city_name :: ByteString
                 , long      :: Double
                 , lat       :: Double
                 } deriving (Show,Eq,Ord)

instance IsCity City where
    longitude = long
    latitude = lat
    name = city_name
    newCity name longi lati =
        City { city_name = name
             , long = longi
             , lat = lati
             }
