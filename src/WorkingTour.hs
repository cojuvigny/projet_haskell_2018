{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE OverloadedStrings      #-}
-- |
-- Module      : WorkingTour
-- Copyright   : (c) Corentin Juvigny 2018
-- License     : BSD-style
--
-- Maintainer  : corentin.juvigny@ensiie.fr
-- Stability   : unsure
-- Portability : ~portable
--
--
-- Data used for creating a WorkingTour.
--

module WorkingTour where

import           City
import           Data.ByteString ( ByteString )
import           Data.List
import qualified Data.Map.Strict as Map
import           Data.Maybe
import qualified Data.Set        as Set
import           Tour

import Debug.Trace

class (IsTour t c) => IsWorkingTour s t c | s -> t c where
    -- | Create a new empty tour
    emptyWorkingTour     :: s
    -- | Create a new tour using a tour and a list of cities remaining.
    --   Should not be used if you're tour needs a graph. Used 'newWorkingTour'' insteed
    newWorkingTour       :: InitFlag -> [c] -> s
    newWorkingTour ONE (x:xs) = workingTourFromList [x,x] xs
    newWorkingTour HULL [s] = workingTourFromList [s,s] []
    newWorkingTour HULL lx =
        workingTourFromList (lg ++ [head lg]) (lx \\ lg)
        where lg = grahamAlgo ((head l):[pivot]) (tail l)
              pivot = minimumBy (\c1 c2 -> compare (latitude c1) (latitude c2)) lx
              l = sortBy (angleGrahamAlgoCmp pivot) (delete pivot lx)
    -- | Create a new Tour using a tour and a list of cities remaining and a graph
    newWorkingTour'     :: InitFlag -> Graph ByteString -> [c] -> s
    newWorkingTour' ONE g (x:xs) = workingTourFromList' g [x,x] xs
    newWorkingTour' HULL g [s] = workingTourFromList' g [s] []
    newWorkingTour' HULL g lx =
        workingTourFromList' g (lg ++ [head lg]) (lx \\ lg)
        where lg = grahamAlgo ((head l):[pivot]) (tail l)
              pivot = minimumBy (\c1 c2 -> compare (latitude c1) (latitude c2)) lx
              l = sortBy (angleGrahamAlgoCmp pivot) (delete pivot lx)
    -- | Add a new step into the tour following the incorporating flag; must not be used with RANDOM flags
    addStep              :: IncorporationFlag -> s -> s
    -- | Does the same thing that addStep but support RANDOM flag. A list of random number must be pass as arguments
    addStep'             :: IncorporationFlag -> s -> Int -> s
    -- | Add new steps until the number of remaining cities is 0. Doesn't work with the flags RANDOM
    finishTour           :: IncorporationFlag -> s -> s
    finishTour flag s
        | isFinishWorkingTour s = s
        | otherwise             = finishTour flag $ addStep flag s
    -- | Same as '@finishTour' but need a list of random number for random draw
    finishTour'          :: IncorporationFlag -> [Int] -> s -> s
    finishTour' RANDOM (x:xs) s
        | isFinishWorkingTour s = s
        | otherwise             = finishTour' RANDOM xs (addStep' RANDOM s x)
    finishTour' flag _ s = finishTour flag s
    -- | Return True if no cities remain in workingTour, else False
    isFinishWorkingTour  :: s -> Bool
    -- | Get the tour of given IsWorkingTour
    getTour              :: s -> t
    -- | Get the remaing cities of the given IsWorkingTour
    getWorkingCities     :: s -> [c]
    -- | Create a new Working tour from two lists of cities
    workingTourFromList  :: [c] -> [c] -> s
    -- | Create a new Working tour from two lists of cities and a graph
    workingTourFromList' :: Graph ByteString -> [c] -> [c] -> s
    -- | Return the nearest remaining city from the workingTour
    getNearestNeighbour  :: s -> (c,[c])
    -- | Return the furthest remaining city from the workingTour
    getFurthestNeighbour :: s -> (c,[c])
    -- |Return a random remaining city.
    getRandomNeighbour   :: s -> Int -> c

-- | Partie I

data WorkingTour t c = WorkingTour (t c) [c] deriving (Show)

instance IsWorkingTour (WorkingTour Tour City) (Tour City) City where
    emptyWorkingTour = WorkingTour emptyTour []

    addStep NEAREST s =
        WorkingTour nwt otherCities
        where t = getTour s
              nwt          = fromJust $ addCity t cityToInsert
              cityToInsert = fst nearestCity
              otherCities  = snd nearestCity
              nearestCity  = getNearestNeighbour s
    addStep FARTHEST s =
        WorkingTour nwt otherCities
        where t = getTour s
              nwt          = fromJust $ addCity t cityToInsert
              cityToInsert = fst farthestCity
              otherCities  = snd farthestCity
              farthestCity = getFurthestNeighbour s
    addStep RANDOM s = error "AddStep must not be used with RANDOM. Use addStep' instead."

    addStep' NEAREST s _     = addStep NEAREST s
    addStep' FARTHEST s _    = addStep FARTHEST s
    addStep' RANDOM s@(WorkingTour t rt) r =
        WorkingTour nwt otherCities
        where cityToInsert = getRandomNeighbour s r
              otherCities  = cityToInsert `delete`rt
              nwt          = fromJust $ addCity t cityToInsert

    isFinishWorkingTour (WorkingTour _ []) = True
    isFinishWorkingTour _                  = False

    getTour (WorkingTour t _) = t

    getWorkingCities (WorkingTour _ r) = r

    workingTourFromList lp lr = WorkingTour (tourFromList lp) lr

    workingTourFromList' g lp lr = WorkingTour (tourFromList' g lp) lr

    getNearestNeighbour = getNeighbour (\ct c1 c2 -> distance' c1 ct < distance' c2 ct)

    getFurthestNeighbour = getNeighbour (\ct c1 c2 -> distance' c1 ct > distance' c2 ct)

    getRandomNeighbour (WorkingTour _ []) r = error "list is null"
    getRandomNeighbour (WorkingTour _ ts) r = ts!!(r `mod` (length ts))

-- | Return a tuple containing the best city according to the fcmp function, and a list containing all of the remaining cities
getNeighbour :: (IsWorkingTour s t c) => (Point -> c -> c -> Bool) -> s -> (c,[c])
getNeighbour fcmp lc =
    foldr (\c (cm,cl) -> let cc = getBarycenter tlc in if fcmp cc c cm
                                                       then (c,cm:cl)
                                                       else (cm,c:cl)
          ) (head rlc,[]) (tail rlc)
    where rlc = getWorkingCities lc
          tlc = getTour lc


-- | Partie II

data WorkingTourGraph t c = WorkingTourGraph (t c) [c] (Set.Set c) deriving (Show)

instance IsWorkingTour (WorkingTourGraph TourGraph City) (TourGraph City) City where
    emptyWorkingTour = WorkingTourGraph emptyTour [] Set.empty

    getTour (WorkingTourGraph t _ _) = t
    getWorkingCities (WorkingTourGraph _ c _) = c

    workingTourFromList lp lr = WorkingTourGraph (tourFromList lp) lr (Set.fromList lp)

    workingTourFromList' g lp lr = WorkingTourGraph (tourFromList' g lp) lr (Set.fromList lp)

    addStep NEAREST s@(WorkingTourGraph t rt g) =
        WorkingTourGraph nwt otherCities (Set.insert cityToInsert g)
        where nwt = case addCity t cityToInsert of
                         Just c  -> c
                         Nothing -> error "Impossible case : the city must be addable"
              cityToInsert = nearestCity
              otherCities  = nearestCity `delete` rt
              nearestCity  = fst $ getNearestNeighbour s
    addStep FARTHEST s@(WorkingTourGraph t rt g) =
        WorkingTourGraph nwt otherCities $ Set.insert cityToInsert g
        where nwt = case addCity t cityToInsert of
                         Just c  -> c
                         Nothing -> error "Impossible case : the city must be addable"
              cityToInsert = farthestCity
              otherCities  = farthestCity `delete` rt
              farthestCity = fst $ getFurthestNeighbour s
    addStep RANDOM s = error "addStep must not be used with random"

    addStep' NEAREST s _  = addStep NEAREST s
    addStep' FARTHEST s _ = addStep FARTHEST s
    addStep' RANDOM s@(WorkingTourGraph t rt g) r =
        case addCity t cityToInsert of
             Just c  -> WorkingTourGraph c otherCities (Set.insert cityToInsert g)
             Nothing -> addStep' RANDOM s (r+1)
        where cityToInsert = getRandomNeighbour s r
              otherCities  = cityToInsert `delete`rt

    isFinishWorkingTour (WorkingTourGraph _ [] _) = True
    isFinishWorkingTour _                         = False

    getNearestNeighbour s = (getNeighbourGraph (\ct c1 c2 -> distance' c1 ct < distance' c2 ct) s,[])
    getFurthestNeighbour s = (getNeighbourGraph (\ct c1 c2 -> distance' c1 ct > distance' c2 ct) s,[])
    getRandomNeighbour (WorkingTourGraph _ ts _) r = ts!!(r `mod` (length ts))

-- | Return the best city according to the fcmp function in the remaining cities in the WorkingTourGraph
getNeighbourGraph :: IsCity c => (Point -> c -> c -> Bool) -> WorkingTourGraph TourGraph c -> c
getNeighbourGraph fcmp s@(WorkingTourGraph t rt g) =
    fst $ foldr (\c (cm,b) -> let cc = getBarycenter t in
                              let lik = isLinked c g in
                              if fcmp cc c cm && lik || not b
                                    then (c,lik)
                                    else (cm,b)
                ) (hrt,isLinked hrt g) trt
    where isLinked c g = Set.foldr (\e ebx -> let result = foldr (\i ecx -> e == name i || ecx) False g in
                                              if result then True || ebx else False || ebx
                                   ) False (connected c)
          connected c = case graph Map.!? (name c) of
                             Just s  -> s
                             Nothing -> Set.empty
          Graph graph = getGraph t
          hrt = head rt
          trt = tail rt
