{-# LANGUAGE CPP                    #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE OverloadedStrings      #-}
-- |
-- Module      : Tour
-- Copyright   : (c) Corentin Juvigny 2018
-- License     : BSD-style
--
-- Maintainer  : corentin.juvigny@ensiie.fr
-- Stability   : unsure
-- Portability : ~portable
--
--
-- Data representing a Tour.
--
--
module Tour where

import           City
import           Control.Monad
import           Control.Parallel
import           Control.Parallel.Strategies
import           Data.ByteString             ( ByteString )
import qualified Data.ByteString.Char8       as B
import           Data.List
import           Data.List.Extra
import qualified Data.Map.Strict             as Map
import           Data.Maybe
import qualified Data.Set                    as Set
import           Data.Tuple.Extra
import           Data.Tuple.Select
import           System.Random

#define INFINITY (1/0)

data InitFlag = ONE | HULL deriving (Show,Read,Eq)

data IncorporationFlag = RANDOM | NEAREST | FARTHEST deriving (Show,Read,Eq)

data OptimisationFlag = REPOSITIONNEMENT | INVERSION deriving (Show,Read,Eq)

newtype Graph a = Graph (Map.Map a (Set.Set a)) deriving (Show)

-- | Return True if graph is Null, False else
grphIsNull :: Graph a -> Bool
grphIsNull (Graph g) = Map.null g

-- | Return a list of n random number between 0 to n
randomSeedDraw :: Int -> IO [Int]
randomSeedDraw n = replicateM n $ randomRIO (0,n)

--Interface
class (IsCity c) => IsTour s c | s -> c where
    -- | /O(1)/ an empty tour
    emptyTour            :: s
    -- |/O(1)/ create a tour containing a single element
    singleton            :: c -> s
    -- | /O(1)/ True if the tour is empty, false otherwise)
    isEmpty              :: s -> Bool
    -- | Get the distance of the tour
    getDistance          :: s -> Double
    -- | Get the barycenter of the tour
    getBarycenter        :: s -> Point
    -- | Return True if the tour contains a graph representing the connection between the cities,
    --   else False
    hasGraph             :: s -> Bool
    -- | Return the graph of s. If it doesn't exist, an error is thrown
    getGraph             :: s -> Graph ByteString
    -- | Return an Tour with a graph added in the structure tour
    --   Return an exception if the notion of graph didn't appear in the tour
    addGraph             :: Graph ByteString -> s -> s
    -- | Create a list of cities representing the Tour
    tourToList           :: s -> [c]
    -- | Create a new tour from two list, the first respresenting a legal tourm
    --   the second the other cities which have to be added to the tour.
    tourFromList         :: [c] -> s
    -- | As '@tourFromList, but need a notion of graph
    tourFromList'        :: Graph ByteString -> [c] -> s
    -- | Add the city inside the tour. If city can't be added, return Nothing
    addCity              :: s -> c -> Maybe s
    -- | Return the total length of the tour if it is not empty, else Nothing
    lengthTour           :: s -> Double
    -- | Return the number of cities in the tour
    nbrElemTour          :: s -> Int
    nbrElemTour = length . tourToList
    -- | Optimize the Tour using the method define by the flag
    optimizeTour         :: OptimisationFlag -> s -> s
    -- | Print the given tour according to the following configuration
    --   distance : city1 city2 ... cityn
    printTour            :: s -> IO ()
    printTour t =
        B.putStrLn $ B.pack (show d) `B.append` " :" `B.append` cities
        where d      = lengthTour t
              l      = tourToList t
              cities = foldl' (\ebx c -> ebx `B.append` " " `B.append` (name c)) "" l


-- | Compare the cities following the angle between them
angleGrahamAlgoCmp :: IsCity c => c -> c -> c -> Ordering
angleGrahamAlgoCmp cp c1 c2
    | a1 < a2   = LT
    | a1 > a2   = GT
    | otherwise = if longitude c1 < longitude c2 then LT else GT
    where a1 = atan2 (latitude c1 - latitude cp) (longitude c1 - longitude cp)
          a2 = atan2 (latitude c2 - latitude cp) (longitude c2 - longitude cp)

-- | Return the convexe hull
--   The first list should be the list e:[pivot] where pivot is the point with the minimum latitude,
--   is e the first element of ll and l the tail of ll, where ll is a list of city sorted by 'angleGrahamAlgoCmp'
grahamAlgo :: IsCity c => [c] -> [c] -> [c]
grahamAlgo [] _ = error "grahamAlgo must be called with the first list not null"
grahamAlgo pile [] = pile
grahamAlgo [p] (x:xs) = grahamAlgo (x:[p]) xs
grahamAlgo pile@(p:ps:px) l@(x:xs)
    | length pile >= 2 && ccw ps p x <= 0 || p == x = grahamAlgo (ps:px) l
    | otherwise = grahamAlgo (x:pile) xs

-- * Partie I

-- | Tour => Ensemble de City & distance totale du tour & barycentre des points

data Tour c = Tour [c] Double Point deriving (Show,Eq)

instance IsCity c => IsTour (Tour c) c where
    emptyTour = Tour [] 0 (0,0)

    singleton c = Tour [c,c] 0 (longitude c,latitude c)

    isEmpty (Tour [] _ _) = True
    isEmpty (Tour _ _ _)  = False

    getDistance   (Tour _ d _) = d
    getBarycenter (Tour _ _ b) = b

    hasGraph t = False
    getGraph t = error "Tour doesn't embed the notion of graph"
    addGraph t = error "Tour doesn't embed the notion of graph"


    tourToList (Tour l _ _) = l

    tourFromList l@[c] = Tour l 0 (longitude c,latitude c)
    tourFromList l     = Tour l dist pos
        where dist   = sum $ zipWith distance (init l) (tail l)
              pos    = dist `par` (calc longitude l / d,calc latitude l / d)
              d      = genericLength l
              calc f = foldl' (\edx d -> f d + edx) 0

    tourFromList' g = tourFromList

    addCity (Tour [v] d (xa,xb)) c =
        Just $ Tour ([v] ++ [c]) (distance v c) ((longitude v + longitude c)/2,(latitude v + latitude c)/2)
    addCity (Tour lv d (xg,yg)) c =
        Just $ Tour mintour (d + nwd) ((n-1)*xg/n + (1/n)*xc,(n-1)*yg/n + (1/n)*yc)
        where mintour = snd3 findbesttour
              nwd = fst3 findbesttour
              xc = longitude c
              yc = latitude c
              n = fromIntegral $ length lv
              findbesttour = foldr (\nwc (df,ln,oldl@(oldc:_)) -> let val = ccd oldc nwc c in
                                                         if val < df
                                                         then (val,nwc:c:oldl,nwc:oldl)
                                                         else (df,nwc:ln,nwc:oldl)
                                   ) (INFINITY,[],[last lv]) (init lv)

    lengthTour (Tour _ d _) = d

    optimizeTour REPOSITIONNEMENT t =
        let optimizeTourArg t cmp =
                if not hasChanged || cmp > 50 then optimizet
                                              else optimizeTourArg optimizet (succ cmp)
                    where hasChanged = abs (lengthTour optimizet - lengthTour t) < 10e-5 --When a fix point is found you can stop
                          optimizet  = nodeRepositioningAlgo t 1 m
                          m          = nbrElemTour t - 1
        in optimizeTourArg t 0
    optimizeTour INVERSION t =
        let optimizeTourArg t@(Tour l _ _) cmp =
                if not hasChanged || cmp > 10 then optimizet
                                             else optimizeTourArg optimizet (succ cmp)
                    where hasChanged = abs (lengthTour optimizet - lengthTour t) < 10e-5 --When a fix point is found you can stop
                          optimizet = tourFromList $ localReverse l []
        in optimizeTourArg t 0

-- | Return an optimazed Tour.
--   This function ensures that the tour is well formed and completed.
--   Requires : n < m else nothing is done.
nodeRepositioningAlgo :: IsCity c => Tour c -> Int -> Int -> Tour c
nodeRepositioningAlgo t@(Tour tr d p) n m
    | n >= m    = t
    | otherwise = if lengthTour nwt - lengthTour t < 0
                  then nodeRepositioningAlgo nwt np m
                  else nodeRepositioningAlgo t np m
        where np       = succ n
              Just nwt = addCity mdt b
              a        = tr'!!0
              b        = tr'!!1
              c        = tr'!!2
              tr'      = drop (n-1) tr
              mdt      = Tour (delete b tr) (d - distance a b - distance b c + distance a c) p

-- | Return an optimized list of cities.
--   Requires : the second list must be empty.
localReverse :: IsCity c => [c] -> [c] -> [c]
localReverse l@(x:xs) q
    | length l < 4 = q ++ l
    | otherwise    = localReverse nxs (q ++ [nx])
    where nxs = tail nwl
          nx  = head nwl
          nwl = localTreatment l 2 l INFINITY

-- | Return the best inversion.
--   Requires : pos > 1, l == lbest, dbest == INFINITY
localTreatment :: IsCity c => [c] -> Int -> [c] -> Double -> [c]
localTreatment l@(a:b:q) pos lbest dbest
    | pos >= length q = lbest
    | otherwise       = if olddist - newdist > 0 && newdist - dbest < 0
                            then localTreatment l (pos + 1) (newsec ++ remaining) newdist
                            else localTreatment l (pos + 1) lbest dbest
    where oldsec    = take pos l
          newsec    = a:c:(reverse (tail (tail (take pos l))))++[b]
          c         = l!!pos
          d         = l!!(pos+1)
          remaining = drop (pos+1) l
          olddist   = distance a b + distance c d
          newdist   = distance a c + distance b d

-- * Partie II

data TourGraph c = TourGraph [c] Double Point (Graph ByteString) deriving (Show)

instance IsCity c => IsTour (TourGraph c) c where
    emptyTour = TourGraph [] 0 (0,0) (Graph Map.empty)

    singleton c = TourGraph [c] 0 (longitude c,latitude c) (Graph Map.empty)

    isEmpty (TourGraph [] _ _ _) = True
    isEmpty _                    = False

    getDistance   (TourGraph _ d _ _) = d
    getBarycenter (TourGraph _ _ b _) = b

    hasGraph t = True
    getGraph (TourGraph _ _ _ g) = g
    addGraph g (TourGraph l d p _) = TourGraph l d p g

    tourToList (TourGraph l _ _ _) = l

    tourFromList l@[c] = TourGraph l 0 (longitude c,latitude c) (Graph Map.empty)
    tourFromList l     = TourGraph l dist pos (Graph Map.empty)
        where dist   = sum $ zipWith distance (init l) (tail l)
              pos    = dist `par` (calc longitude l / d,calc latitude l / d)
              d      = genericLength l
              calc f = foldl' (\edx d -> f d + edx) 0

    tourFromList' g l = addGraph g $ tourFromList l

    addCity (TourGraph [v] d (xa,xb) g) c
        | isLinked v c = Just $ TourGraph ([v] ++ [c]) (distance v c) ((longitude v + longitude c)/2,(latitude v + latitude c)/2) g
        | otherwise    = Nothing
        where isLinked va vb = Set.member (name vb) $ neighbours va
              neighbours v' = mp Map.! (name v')
              Graph mp = g
    addCity (TourGraph lv d (xg,yg) g) c
        | hasBeenAdded = Just $ TourGraph mintour (d+nwd) ((n-1)*xg/n + (1/n)*xc,(n-1)*yg/n + (1/n)*yc) g
        | otherwise    = Nothing
        where mintour = sel2 findbesttour
              nwd = sel1 findbesttour
              hasBeenAdded = sel4 findbesttour
              xc = longitude c
              yc = latitude c
              n  = genericLength lv
              Graph mp = g
              isLinked c nwc = Set.member (name c) $ mp Map.! (name nwc)
              findbesttour    = foldr (\nwc (df,ln,oldl@(oldc:_),bool) -> let val = ccd oldc nwc c in
                                                        if val < df && isLinked c oldc
                                                        then if isLinked c nwc
                                                             then (val,nwc:c:oldl,nwc:oldl,True)
                                                             else (2*(distance oldc c),nwc:oldc:c:oldl,nwc:oldl,True)
                                                        else (df,nwc:ln,nwc:oldl,bool)
                                      ) (INFINITY,[],[last lv],False) (init lv)

    lengthTour (TourGraph _ d _ _) = d

    optimizeTour REPOSITIONNEMENT t =
        let optimizeTourArg t cmp =
                if not hasChanged || cmp > 50 then optimizet
                                              else optimizeTourArg optimizet (succ cmp)
                    where hasChanged = abs (lengthTour optimizet - lengthTour t) < 10e-5 --When a fix point is found you can stop
                          optimizet  = nodeRepositioningGraphAlgo t 1 m
                          m          = nbrElemTour t - 1
        in optimizeTourArg t 0
    optimizeTour INVERSION t =
        let optimizeTourArg t@(TourGraph l _ _ g) cmp =
                if not hasChanged || cmp > 10 then optimizet
                                              else optimizeTourArg optimizet (succ cmp)
                    where hasChanged = abs (lengthTour optimizet - lengthTour t) < 10e-5 --When a fix point is found you can stop
                          optimizet = tourFromList' g $ localReverseGraph l [] g
        in optimizeTourArg t 0

-- | Return an optimazed Tour.
--   This function ensures that the tour is well formed and completed.
--   Requires : n < m else nothing is done.
nodeRepositioningGraphAlgo :: IsCity c => TourGraph c -> Int -> Int -> TourGraph c
nodeRepositioningGraphAlgo t@(TourGraph tr d p g) n m
    | n >= m    = t
    | otherwise = if lengthTour nwt - lengthTour t < 0 && isLinked
                     then nodeRepositioningGraphAlgo nwt np $ if isAlreadyIn then m-1 else m
                     else nodeRepositioningGraphAlgo t np m
    where np    = succ n
          nwt   = if isAlreadyIn && isLinked then mdt else added
          added = case addCity mdt b of
                     Just l  -> l
                     Nothing -> TourGraph [] INFINITY (0.0,0.0) g
          isAlreadyIn = any (==b) nwtr
          isLinked    = Set.member (name c) $ mp Map.! (name a)
          Graph mp    = g
          a    = tr'!!0
          b    = tr'!!1
          c    = tr'!!2
          tr'  = drop (n-1) tr
          mdt  = TourGraph nwtr (d - distance a b - distance b c + distance a c) p g
          nwtr = (take n tr) ++ (drop (n+1) tr)

-- | Return an optimized list of cities.
--   Requires : the second list must be empty, g should be not null.
localReverseGraph :: IsCity c => [c] -> [c] -> Graph ByteString -> [c]
localReverseGraph l@(x:xs) q g
    | length l < 4 = cleaningList (q ++ l)
    | otherwise    = localReverseGraph nxs (q ++ [nx]) g
        where nxs = tail nwl
              nx  = head nwl
              nwl = localTreatmentGraph l 2 l INFINITY g

-- | Return the best inversion.
--   Requires : pos > 1, l == lbest, dbest == INFINITY, graph should be not null.
localTreatmentGraph :: IsCity c => [c] -> Int -> [c] -> Double -> Graph ByteString -> [c]
localTreatmentGraph l@(a:b:q) pos lbest dbest g
    | pos > length q = lbest
    | otherwise      = if olddist - newdist > 0 && newdist - dbest < 0 && areConnected
                            then localTreatmentGraph l (pos + 1) (newsec ++ remaining) newdist g
                            else localTreatmentGraph l (pos + 1) lbest dbest g
    where oldsec    = take pos l
          newsec    = a:c:(reverse (tail (tail (take pos l))))++[b]
          c         = l!!pos
          d         = l!!(pos+1)
          Graph mp  = g
          remaining = drop (pos+1) l
          olddist   = distance a b + distance c d
          newdist   = distance a c + distance b d
          areConnected = Set.member (name a) set1 && Set.member (name d) set2
          set1 = case mp Map.!? (name c) of
                      Just v  -> v
                      Nothing -> error $ "City unknown"
          set2 = case mp Map.!? (name b) of
                      Just v  -> v
                      Nothing -> error $ "City unknown"

cleaningList :: Eq c => [c] -> [c]
cleaningList l =
    snd $ foldr (\e ebx@(pred,ls) -> if e == pred then ebx else (e,e:ls)) (lt,[lt]) it
        where lt = last l
              it = init l
{-# INLINE cleaningList #-}
