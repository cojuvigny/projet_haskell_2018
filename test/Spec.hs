{-# LANGUAGE OverloadedStrings, NoImplicitPrelude #-}

import qualified System.IO as IO
import Prelude hiding ( putStrLn
                      , putStr
                      , getChar
                      , getLine
                      , getContents
                      , interact
                      )
import qualified Data.Set as Set
import Control.Parallel
import Data.Foldable
import Data.List
import Data.ByteString.Char8 hiding (all,any,map,length)
import qualified Data.Text.IO as TIO
import Data.Text.Encoding
import System.Environment
import System.Random
import System.Exit
import City
import Tour
import WorkingTour
import ReadFiles hiding (hlred,hlgreen)

hlred :: ByteString -> ByteString
hlred s = "\x1B[1m\x1B[31m" +++ s +++ "\x1B[0m"

hlgreen :: ByteString -> ByteString
hlgreen s = "\x1B[1m\x1B[32m" +++ s +++ "\x1B[0m"

type Test b = (ByteString, b, b)

(+++) :: ByteString -> ByteString -> ByteString
a +++ b = append a b

do_test :: (Show a, Eq a) => Test a -> IO.IO ()
do_test (text, v1, v2)
    | v1 == v2  = putStrLn $ text +++ ":\t\t " +++ hlgreen "Pass"
    | otherwise = do putStrLn $ text +++ ":\t\t " +++ hlred "Fail\t expected =" +++ (pack $ show v2)
                     exitFailure
-- | Test if all cities in l ar in t and that it as the right length
all_in :: IsCity c => [c] -> [c] -> Bool
all_in t l = all (\e -> any (==e) t) l && length l + 1 == length t

-- Vérifie que la distance indiquée par le tour et la distance calculée dans le tour sont identiques
hasCorrectLength :: (IsTour (t c) c, IsCity c) => t c -> Bool
hasCorrectLength t = lengthTour t == sum (Data.List.zipWith distance (Data.List.init  (tourToList t)) (Data.List.tail (tourToList t)))


main = do
    putStrLn ("" :: ByteString)
    let villea = City { city_name      = "Paris"
                      , long           = 2.3488000
                      , lat            = 48.8534100
                      }
    let villeb = City { city_name      = "Pont-St-Mard"
                      , long           = 3.1709
                      , lat            = 49.2953
                      }
    let villec = City { city_name      = "London"
                      , long           = 0.0739
                      , lat            = 51.3026
                      }
    let voidTour = emptyTour :: Tour City 
    let tour1 = tourFromList [villea,villeb,villec] :: Tour City
    let workingTour1 = workingTourFromList [villea] [villeb,villec] :: WorkingTour Tour City

    (readFile, graphFile) <- readCityFile "test/ressources/testCity.txt" -- "test/ressources/ville.txt"
    readSpec <- readFormatFile "test/ressources/testFormat.txt"
    rdseed   <- randomSeedDraw (Data.List.length readFile)

    putStrLn $ hlred "Test unitaire City"
    let testcity = [ ("Distance Paris--Pont-St-Mard\t", abs (distance villea villeb - 0.9333355142176903) < 10e-8, True)
                   , ("Distance Pont-St-Mard--Londres\t", abs (distance villeb villec - sqrt ((49.2953 - 51.3026)^2 + (3.1709 - 0.0739)^2)) < 10e-8, True)
                   , ("ccw Paris--Londres--Pont-St-Mard", ccd villea villeb villec - ((distance villea villec + distance villeb villec) - distance villea villeb) < 10e-8, True)
                   ]
    traverse_ do_test testcity

    putStrLn $ hlred "\nTest unitaire Tour"
    let testtour =  [ ("isEmpty voidTour\t", isEmpty voidTour, True)
                    , ("not isEmpty tour1\t", isEmpty tour1, False)
                    , ("length tour1\t\t", let dist = lengthTour tour1 in abs (dist - (distance villea villeb + distance villeb villec)) < 10e-8, True)
                    , ("length null == 0\t", lengthTour voidTour == 0, True)
                    , ("cmp graham correct1\t", angleGrahamAlgoCmp villea villeb villec == LT, True)
                    , ("cmp graham correct2\t", angleGrahamAlgoCmp villea villec villeb == GT, True)
                    , ("graham algo is correct\t",grahamAlgo [villeb,villea] [villec] == [villec,villeb,villea], True)
                    , ("tourFromList\t\t",tourFromList [villea] == Tour [villea] 0.0 (2.3488,48.85341), True)
                    , ("lengthTour\t\t",lengthTour (tourFromList [villea,villeb] :: Tour City) == distance villea villeb, True)
                    ]
    traverse_ do_test testtour
    putStrLn $ hlred "\nTest unitaire TourGraph"
    let testtourgraph =  [ ("isEmpty voidTour\t", isEmpty voidTour, True)
                    , ("not isEmpty tour1\t", isEmpty tour1, False)
                    , ("length tour1\t\t", let dist = lengthTour tour1 in abs (dist - (distance villea villeb + distance villeb villec)) < 10e-8, True)
                    , ("length null == 0\t", lengthTour voidTour == 0, True)
                    , ("cmp graham correct1\t", angleGrahamAlgoCmp villea villeb villec == LT, True)
                    , ("cmp graham correct2\t", angleGrahamAlgoCmp villea villec villeb == GT, True)
                    , ("graham algo is correct\t",grahamAlgo [villeb,villea] [villec] == [villec,villeb,villea], True)
                    , ("tourFromList\t\t",tourFromList [villea] == Tour [villea] 0.0 (2.3488,48.85341), True)
                    , ("lengthTour\t\t",lengthTour (tourFromList [villea,villeb] :: TourGraph City) == distance villea villeb, True)
                    ]
    traverse_ do_test testtourgraph

    --print readFile
    --print readSpec
    -- Partie I
    --putStrLn $ hlred "\nPivot :"
    let nwWorkingtour = newWorkingTour HULL readFile :: WorkingTour Tour City
    let pivot = minimumBy (\c1 c2 -> compare (latitude c1) (latitude c2)) readFile
    --print pivot
    
    --putStrLn $hlred "\nAll except pivot :"
    --print $ sortBy (angleGrahamAlgoCmp pivot) (delete pivot readFile)
    
    --putStrLn $ hlred "\nnewTour :"
    --print nwWorkingtour

    --putStr $ hlred "NEAREST city : "
    --let nearestCity = getNEARESTNeighbour nwWorkingtour
    --print nearestCity
    --putStrLn $ hlred "Relative distance : " +++ pack (show (distance' (fst nearestCity) (getBarycenter (getTour nwWorkingtour)))) +++ Data.List.foldl' (\s c -> s +++ " " +++ pack (show (distance' c (getBarycenter (getTour nwWorkingtour))))) "" (snd nearestCity)

    --putStr $ hlred "Furthest city : "
    --let furthestCity = getFurthestNeighbour nwWorkingtour
    --print furthestCity
    --putStrLn $ hlred "Relative distance : " +++ pack (show (distance' (fst furthestCity) (getBarycenter (getTour nwWorkingtour)))) +++ Data.List.foldl' (\s c -> s +++ " " +++ pack (show (distance' c (getBarycenter (getTour nwWorkingtour))))) "" (snd furthestCity)

    --let nwWorkingtour2 = newWorkingTour ONE readFile :: WorkingTour Tour City
    --putStrLn $ hlred "\nnewWorkingTour ONE"
    --print nwWorkingtour2
    --putStrLn $ hlred "Test ajout d'une city par le NEAREST"
    --print $ addStep NEAREST nwWorkingtour2
    --putStrLn $ hlred "Test de creation d'un tour par le nearest"
    --let finisht = finishTour NEAREST nwWorkingtour2
    --print finisht
    --let Tour finishtour _ _ = getTour finisht
    --putStr $ hlgreen "Veritable distance du tour : "
    --print . sum $ Data.List.zipWith distance (Data.List.init finishtour) (Data.List.tail finishtour) 
    --putStrLn $ hlred "Test ajout d'une city par le Farthest"
    --let finisht' = finishTour FARTHEST nwWorkingtour2
    --print finisht'
    --let Tour finishtour' _ _ = getTour finisht'
    --putStr $ hlgreen "Veritable distance du tour : "
    --print . sum $ Data.List.zipWith distance (Data.List.init finishtour') (Data.List.tail finishtour') 
    putStrLn $ hlred "\nPartie I\n"
    
    --NEAREST
    putStrLn $ hlred "\nnewWorkingTour HULL"
    let nwWorkingtour3 = newWorkingTour HULL readFile :: WorkingTour Tour City
    putStrLn $ hlred "Test creation par le plus proche"
    let finisht2 = finishTour NEAREST nwWorkingtour3
    --print finisht2
    let Tour finishtour2 _ _ = getTour finisht2
    TIO.putStr . decodeUtf8 $ hlgreen "Veritable distance du tour : "
    print . sum $ Data.List.zipWith distance (Data.List.init finishtour2) (Data.List.tail finishtour2) 
    putStr $ hlgreen "Test optimisation par repositionnement de noeuds : "
    print . lengthTour . optimizeTour REPOSITIONNEMENT . getTour $ finisht2
    --putStr $ hlgreen "Test optimisation par inversion locale : "
    --print . lengthTour . optimizeTour INVERSION . getTour $ finisht2
    --Farthest
    putStrLn $ hlred "Test ajout d'une city par le Farthest"
    let finisht2' = finishTour FARTHEST nwWorkingtour3
    --print finisht2'
    let Tour finishtour2' _ _ = getTour finisht2'
    putStr $ hlgreen "Veritable distance du tour : "
    let tt2 = lengthTour . optimizeTour REPOSITIONNEMENT . getTour $ finisht2'
    tt2 `par` print . sum $ Data.List.zipWith distance (Data.List.init finishtour2') (Data.List.tail finishtour2') 
    putStr $ hlgreen "Test optimisation par repositionnement de noeuds : "
    print tt2
    --putStr $ hlgreen "Test optimisation par inversion locale : "
    --print . lengthTour . optimizeTour INVERSION . getTour $ finisht2'
    --RANDOM
    putStrLn $ hlred "Test ajout d'une city par l'aleatoire"
    let finisht2'' = finishTour' RANDOM rdseed nwWorkingtour3
    --print finisht2''
    let Tour finishtour2'' _ _ = getTour finisht2''
    putStr $ hlgreen "Veritable distance du tour : "
    print . sum $ Data.List.zipWith distance (Data.List.init finishtour2'') (Data.List.tail finishtour2'') 
    putStr $ hlgreen "Test optimisation par repositionnement de noeuds : "
    let optimize'' = optimizeTour REPOSITIONNEMENT . getTour $ finisht2''
    print . lengthTour $ optimize''
    let Tour optimizet'' _ _ = optimize''
    putStr $ hlgreen "All in t : "
    print $ all_in optimizet'' readFile
    putStr $ hlgreen "Test optimisation par inversion locale : "
    let optimize2'' = optimizeTour INVERSION . getTour $ finisht2''
    print . lengthTour $ optimize2''
    let Tour optimizet2'' _ _ = optimize2''
    putStr $ hlgreen "All in t : "
    print $ all_in optimizet2'' readFile
    putStr $ hlgreen "Has correct length t : "
    print $ hasCorrectLength optimize2''

    --Partie II
    putStrLn $ hlred "\nPartie II\n"
    let testworkingtournotgraph = newWorkingTour ONE readFile :: WorkingTour Tour City
    --print . optimizeTour INVERSION . getTour . finishTour' NEAREST rdseed $ testworkingtournotgraph
    let newWorkingTourGraph = newWorkingTour' ONE graphFile readFile :: WorkingTourGraph TourGraph City
    --print newWorkingTourGraph
    putStrLn $ hlgreen "without optimization"
    printTour . getTour . finishTour' NEAREST rdseed $ newWorkingTourGraph
    putStrLn $ hlgreen "with optimization"
    printTour . optimizeTour REPOSITIONNEMENT . getTour . finishTour' NEAREST rdseed $ newWorkingTourGraph
    --printTour . optimizeTour INVERSION . getTour . finishTour' RANDOM rdseed $ newWorkingTourGraph
    
    
    exitSuccess
